from django.conf.urls import url

from mysite.core import views
from rest_framework.routers import SimpleRouter


urlpatterns = [
    url(r'^$', views.UsersListView.as_view(), name='users_list'),
    url(r'^users$', views.question_list),
url(r'^cached_questions', views.view_cached_questions),

    url(r'^generate/$', views.GenerateRandomUserView.as_view(), name='generate'),
]
