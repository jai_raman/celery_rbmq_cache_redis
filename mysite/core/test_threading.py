import time
import threading
import os

# def cal_square(array):
#     print 'squares'
#     for i in array:
#         time.sleep(0.2)
#         print (i*i,'square')
#
# def cal_cube(array):
#     print 'cubes'
#     for i in array:
#         time.sleep(0.2)
#         print (i*i*i,'cube')
#
# array = [1,2,3,4]
#
# t =  time.time()
#
# t1 = threading.Thread(target = cal_square,args = (array,))
# t2 = threading.Thread(target = cal_cube,args = (array,))
#
# t1.start()
# t2.start()
#
# t1.join()
# t2.join()
#
#
# print 'time taken %s'%(time.time()-t)
#

# Python program to illustrate the concept
# of threading


# def task1():
#     print("Task 1 assigned to thread: {}".format(threading.current_thread().name))
#     print("ID of process running task 1: {}".format(os.getpid()))
#
#
# def task2():
#     print("Task 2 assigned to thread: {}".format(threading.current_thread().name))
#     print("ID of process running task 2: {}".format(os.getpid()))
#
#
# if __name__ == "__main__":
#     # print ID of current process
#     print("ID of process running main program: {}".format(os.getpid()))
#
#     # print name of main thread
#     # print("Main thread name: {}".format(threading.main_thread().name))
#
#     # creating threads
#     t1 = threading.Thread(target=task1,name = 't1')
#     t2 = threading.Thread(target=task2,name = 't2')
#
#     # starting threads
#     t1.start()
#     t2.start()
#
#     # wait until all threads finish
#     t1.join()
#     t2.join()







#
# # global variable x
# x = 0
#
#
# def increment():
#     """
#     function to increment global variable x
#     """
#     global x
#     x += 1
#
#
# def thread_task():
#     """
#     task for thread
#     calls increment function 100000 times.
#     """
#     for _ in range(100000):
#         increment()
#
#
# def main_task():
#     global x
#     # setting global variable x as 0
#     x = 0
#
#     # creating threads
#     t1 = threading.Thread(target=thread_task)
#     t2 = threading.Thread(target=thread_task)
#
#     # start threads
#     t1.start()
#     t2.start()
#
#     # wait until threads finish their job
#     t1.join()
#     t2.join()
#
#
# if __name__ == "__main__":
#     for i in range(10):
#         main_task()
#         print("Iteration {0}: x = {1}".format(i, x))




import threading

# global variable x
# x = 0
#
#
# def increment():
#     """
#     function to increment global variable x
#     """
#     global x
#     x += 1
#
#
# def thread_task(lock):
#     """
#     task for thread
#     calls increment function 100000 times.
#     """
#     for _ in range(100000):
#
#         lock.acquire()
#         increment()
#         lock.release()
#
# def main_task():
#     global x
#     # setting global variable x as 0
#
#     x = 0
#     # creating a lock
#     lock = threading.Lock()
#
#     # creating threads
#     t1 = threading.Thread(target=thread_task, args=(lock,))
#     t2 = threading.Thread(target=thread_task, args=(lock,))
#     # t3 = threading.Thread(target=thread_task, args=(lock,))
#     # start threads
#     t1.start()
#     t2.start()
#     # t3.start()
#     # wait until threads finish their job
#     t1.join()
#     t2.join()
#
# if __name__ == "__main__":
#     t = time.time()
#     for i in range(10):
#         main_task()
#         print("Iteration {0}: x = {1}".format(i, x))
#     print 'time _taken %s' %(time.time()-t)

# t = time.time()
#
#
# def inc():
#     for i in range(1000):
#         print i
#
#
#
# print time.time() - t
#
# t1 = threading.Thread(target = inc)
# t2 = threading.Thread(target = inc)
#
# t1.start()
# t2.start()
#
# t1.join()
# t2.join()


# import multiprocessing
#
# print multiprocessing.cpu_count()

from multiprocessing import Queue

# colors = ['red', 'green', 'blue', 'black']
# cnt = 1
# # instantiating a queue object
# queue = Queue()
# print('pushing items to queue:')
# for color in colors:
#     print('item no: ', cnt, ' ', color)
#     queue.put(color)
#     cnt += 1
#
# print('\npopping items from queue:')
# cnt = 1
# while not queue.empty():
#     print('item no: ', cnt, ' ', queue.get())
#     cnt += 1

# from multiprocessing import Lock, Process, Queue, current_process
# import time
#
#  # imported for using queue.Empty exception
#
#
# def do_job(tasks_to_accomplish, tasks_that_are_done):
#     while True:
#         try:
#             '''
#                 try to get task from the queue. get_nowait() function will
#                 raise queue.Empty exception if the queue is empty.
#                 queue(False) function would do the same task also.
#             '''
#             task = tasks_to_accomplish.get_nowait()
#         except queue.Empty:
#
#             break
#         else:
#             '''
#                 if no exception has been raised, add the task completion
#                 message to task_that_are_done queue
#             '''
#             print(task)
#             tasks_that_are_done.put(task + ' is done by ' + current_process().name)
#             time.sleep(.5)
#     return True
#
#
# def main():
#     number_of_task = 10
#     number_of_processes = 4
#     tasks_to_accomplish = Queue()
#     tasks_that_are_done = Queue()
#     processes = []
#
#     for i in range(number_of_task):
#         tasks_to_accomplish.put("Task no " + str(i))
#
#     # creating processes
#     for w in range(number_of_processes):
#         p = Process(target=do_job, args=(tasks_to_accomplish, tasks_that_are_done))
#         processes.append(p)
#         p.start()
#
#     # completing process
#     for p in processes:
#         p.join()
#
#     # print the output
#     while not tasks_that_are_done.empty():
#         print(tasks_that_are_done.get())
#
#     return True
#
#
# if __name__ == '__main__':
#     main()



import urllib2
from multiprocessing.dummy import Pool as ThreadPool

t = time.time()
urls = [
  'http://www.python.org',
  'http://www.python.org/about/',
  'http://www.onlamp.com/pub/a/python/2003/04/17/metaclasses.html',
  'http://www.python.org/doc/',
  'http://www.python.org/download/',
  'http://www.python.org/getit/',
  'http://www.python.org/community/',
  'https://wiki.python.org/moin/',
  ]

# make the Pool of workers
pool = ThreadPool(4)

# open the urls in their own threads
# and return the results
results = pool.map(urllib2.urlopen, urls)

# close the pool and wait for the work to finish
pool.close()
pool.join()
print 'time taken %s' %(time.time() - t)