from django.contrib.auth.models import User
from django.contrib import messages
from django.views.generic import TemplateView
from django.views.generic.list import ListView
from django.views.generic.edit import FormView
from django.shortcuts import redirect

from .forms import GenerateRandomUserForm
from .tasks import create_random_user_accounts
from rest_framework import response,status
from .serializers import *
from rest_framework.decorators import api_view

from django.core.cache.backends.base import DEFAULT_TIMEOUT

from django.views.decorators.cache import cache_page

from django.core.cache import cache
from django.conf import settings

CACHE_TTL = getattr(settings, 'CACHE_TTL', DEFAULT_TIMEOUT)

class UsersListView(ListView):
    template_name = 'core/users_list.html'
    model = User


@api_view(['GET'])
def question_list(request):
    queryset = User.objects.filter(id__lte = 10000)
    print  queryset.count()
    serializer = user_serializer(instance=queryset,many=True)
    return response.Response(serializer.data)


@api_view(['GET'])
def view_cached_questions(request):

    if 'all_questions' in cache:
        # get results from cache
        products = cache.get('all_questions')
        # if  products_count.__len__() == questions.objects.all().count():

        return response.Response(products, status=status.HTTP_201_CREATED)
    else:
        questions_ob = User.objects.filter(id__lte = 10000)
        serializer = user_serializer(instance=questions_ob, many=True)

        # store data in cache
        cache.set('all_questions', serializer.data, timeout=CACHE_TTL)
        return response.Response(serializer.data, status=status.HTTP_201_CREATED)

class GenerateRandomUserView(FormView):
    template_name = 'core/generate_random_users.html'
    form_class = GenerateRandomUserForm

    def form_valid(self, form):
        total = form.cleaned_data.get('total')
        create_random_user_accounts.delay(total)
        messages.success(self.request, 'We are generating your random users! Wait a moment and refresh this page.')
        return redirect('users_list')